#!/bin/bash/
#Fecha: 12-01-2021
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Elabora un programa que compare 3 numeros
#y los ordene de mayor a menor

echo "Digite el primer numero: "
read numero_1
echo "Digite el segundo numero: "
read numero_2
echo "Digite el tercer numero: "
read numero_3

if [ $numero_1 -gt $numero_2 ] && [ $numero_2 -gt $numero_3 ]; then
	echo $numero_1
	echo $numero_2
	echo $numero_3
elif [ $numero_1 -gt $numero_3 ] && [ $numero_3 -gt $numero_3 ]; then
	echo $numero_1
	echo $numero_2
	echo $numero_3
elif [ $numero_2 -gt $numero_1 ] && [ $numero_1 -gt $numero_3 ]; then
	echo $numero_2
	echo $numero_1
	echo $numero_3
elif [ $numero_2 -gt $numero_3 ] && [ $numero_3 -gt $numero_1 ]; then
	echo $numero_2
	echo $numero_3
	echo $numero_1
elif [ $numero_3 -gt $numero_1 ] && [ $numero_1 -gt $numero_2 ]; then
	echo $numero_3
	echo $numero_1
	echo $numero_2
elif [ $numero_3 -gt $numero_1 ] && [ $numero_2 -gt $numero_1 ]; then
	echo $numero_3
	echo $numero_2
	echo $numero_1
fi

