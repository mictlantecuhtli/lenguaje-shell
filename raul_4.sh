#!/bin/bash

#Fecha: 19_10_20
#Autor: Fatima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Realice un programa en el cual se realice un descuento
#del 35% a cualquier medicamento

DESCUETO=0.37
total=0
echo "Ingrese el nombre del medicamento: "
read medicamento
echo "Ingrese su precio: "
read precio
	total=$(echo "scale=2;$precio*$DESCUENTO" | bc -l) 
	echo "El total de pagar por su medicamento es de: ".$total." pesos"
