#!/bin/bash
#Fecha: 20-08-2020
#Autor: Fatima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Se requiere determinar el costo que tendrá realizar una llamada te­lefónica con base en el tiempo que dura la llamada y en el costo por minuto.

echo "Ingrese el precio por minuto: "
read precioMinuto
echo "Ingrese los minutos que tomo su llamada: "
read minuto

resultado=$((precioMinuto*minuto))

echo "El costo que tendra su llamada es de: $" $resultado "pesos"
