#!/bin/bash
#Fecha: 28_12_2020
#Autor: Fatima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

#Obtener la edad de una persona en meses, si se ingresa su edad
#en años y meses. Ejemplo: Ingresado 3 años 4 meses debe
#mostrar 40 meses


edad = 0

echo "Ingrese el numero de años que tiene: "
read anyos
echo "Con cuantos meses: "
read meses

edad=$(($edad + $($anyos*12) + $meses))

echo "Su edad en meses es de: "$edad
