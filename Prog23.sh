#!bin/bash

#Fecha:24-08-2020
#Autor:Fatima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Realice un algoritmo para leer las calificaciones de N alumnos y de­termine el número de aprobados y reprobados. 
calificaciones=[]
cambio=0
aprobados=0
noAprobados=0

echo "Ingrese la cantidad de alumnos: "
read numeroAlumnos

for ((x = 0; x < $numeroAlumnos; x++)) do
	cambio=$(($cambio + 1))
	echo "Ingrese la calificacion del alumno: $cambio "
	read calificacion
	calificaciones[$x]=$calificacion
        if [ ${calificaciones[$x]} -ge 6 ] && [ ${calificaciones[$x]} -le 10 ]; then
		echo "Alumno aprobado"
		aprobados=$(($aprobados + 1))
	elif [ ${calificaciones[$x]} -lt 6 ] && [ ${calificaciones[$x]} -gt 0 ]; then
		echo "Alumno no aprobado"
		noAprobados=$((noAprobados + 1))
	fi
done

echo "El total de alumnos aprobados es de: "$aprobados
echo "El total de alumnos reprobados es de: "$noAprobados
