#!/bin/bash/
#Fecha: 04-01-2021
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Calcula la cantidad de dinero mexicano
#a euros y dolares considerando que el
#dolar= 19.95 euro=24.54

EURO=24.54
DOLLAR=19.95
pesoMexicano=0
conversionEuro=0
conversionDollar=0

echo "1)Conversion a dolares" 
echo "2)Conversion a euros"
echo "Digite su opción: "
read opcion

if [ $opcion -eq 1 ]; then
	echo "Usted a elegido convertir su dinero a dolares"
	echo "\nIngrese la cantidad de dinero que desea convertir: "
	read pesoMexicano
	conversionDollar=$(echo "scale=2; ($pesoMexicano*$DOLLAR)" | bc -l)
	echo "Usted tiene: $"$conversionDollar" dolares"
elif [$opcion -eq 2 ]; then
	echo "Usted a elegido convertir su dinero a euros"
        echo "\nIngrese la cantidad de dinero que desea convertir: "
        read pesoMexicano
        conversionEuro=$(echo "scale=2; ($pesoMexicano*$EURO)" | bc -l)
        echo "Usted tiene: €"$conversionEuro" euros"
fi


