#!/bin/bash/
#Fecha: 02-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
#TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE FRECUENTE.
#LES OTORGARÁN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.

total_Ventas=0
ventas_Descuento=()
ventas_no_Descuento=()

echo -e "¿Cuenta con tarjeta de cliente frecuente?:\n\t1)Si\n\t2)No"
echo  "Digite su opcion: "
read opcion

if [ $opcion == 1 ]; then
	echo "Digite el numero de ventas que realizo: "
	read numVentas

	for (( x = 0; x < $numVentas; x++ )) do
		valorVenta=0
		echo "Digte el costo de la venta "$x": "
		read valorVenta
		ventas_Descuento[$x]=$valorVenta
		suma_Ventas=$(($total_Ventas+$valorVenta))
	done
	total_Ventas=$(echo "scale=2;$suma_Ventas-($suma_Ventas*0.20)" | bc -l)
	echo "El monto total es de: $ $total_Ventas pesos"

elif [ $opcion == 2 ]; then
	echo "Digite el numero de ventas que realizo: "
        read numVentas

        for (( x = 0; x < $numVentas; x++ )) do
                echo "Digte el costo de la venta "$i": "
                read valorVenta
                ventas_no_Descuento[$x]=$valorVenta
                total_Venta=$(($total_Venta + ${ventas_no_Descuento[$x]}))
        done
        echo "El monto total es de: $"$total_Venta" pesos"
fi
