#!/bin/bash/
#Fecha: 02-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Area de un gallinero

echo -e "\n\t\t\tB I E N V E N I D O "
echo -e "\tEste programa te ayudara a calcular la medida ideal de tu gallinero"
echo "Digita la altura del terreno en el que desea construir (m): "
read h_Terreno
echo "Digite la base del terreno en el que deseas construir (m): "
read b_Terreno

p_Terreno=$((2*($h_Terreno+$b_Terreno)))

echo "¿Con cuantos metros lineales de material cuenta? (m): "
read material

if [ $material -lt $p_Terreno ]; then
	echo "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
	echo -e "\tBase (m)\tAltura (m)\tLargo (m)\tArea (m)"
	echo "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
	for (( i = 1; i < $(($material/2)); i++ )) do
		altura=$(((50-2*($i))/2))
		area=$(($i*$altura))		
		echo -e "\t"$i"\t\t"$altura"\t\t"$material"\t\t"$area
	done
else
	echo "El material ecxede los limites, lo sentimos"
fi
