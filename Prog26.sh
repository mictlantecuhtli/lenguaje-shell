#!/bin/bash/

#Fecha: 01-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#26-El banco “Bandido de peluche” desea calcular para cada uno de sus N
#clientes su saldo actual, su pago mínimo y su pago para no generar intereses. 
#Además, quiere calcular el monto de lo que ganó por concepto interés con los 
#clientes morosos. Los datos que se conocen de cada cliente son: saldo anterior, 
#monto de las compras que realizó y pago que depositó en el corte anterior. 
#Para calcular el pago mínimo se considera 15% del saldo actual, y el pago para 
#no generar intere­ses corresponde a 85% del saldo actual, considerando que el 
#saldo actual debe incluir 12% de los intereses causados por no realizar el pago
#mínimo y $200 de multa por el mismo motivo. Realice el algo­ritmo correspondiente 
#y represéntelo mediante diagrama de flujo y pseudocódigo.

interes1=.12
interes2=.15
interes3=.85
pagoActual=0
pagoMinimo=0
pagoInteres=0

echo "Inserte la cantidad de clientes: "
read numClientes

for i in $(seq $numClientes);do
	echo "Inserte el nombre del cliente: "
	read nombre
	echo "Inserte el saldo anterior: "
	read saldoAnterior
	echo "Inserte su ultimo deposito: "
	read depositoAnterior
	echo "Inserte el monto por sus compras realizadas: "
	read montoVentas
	echo "Inserte su saldo actual: "
	read saldoActual
	pagoActual=$(echo "scale=2;$saldoActual*$interes1" | bc -l) 
	pagoMinimo=$(echo "scale=2;$saldoActual*$interes2" | bc -l)
	pagoInteres=$(echo "scale=2;$saldoActual*$interes3" | bc -l )
	echo "El pago actual de  " $nombre " es de: $" $pagoActual " pesos"
	echo "El pago minimo de " $nombre " es de: $" $pagoActual " pesos"
	echo "El pago para no generar intereses de " $nombre " es de: $" $pagoInteres " pesos" 
done
