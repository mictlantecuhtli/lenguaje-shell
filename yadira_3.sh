#!/bin/bash/
#Fecha: 07-01-2021
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#UN PROGRAMA SOLICITA QUE SEAN CAPTURADOS TRES DATOS
#NUMÉRICOS Y A PARTIR DE ELLOS IMPRIMIRÁ SI EL NÚMERO ES PAR.

resultado_1=0
resultado_2=0
resultado_3=0

echo "Digite el primer numero: "
read numero_1
echo "Digite el segundo numero: "
read numero_2
echo "Digite el tercer numero: "
read numero_3

resultado_1=$(($numero_1%2))
resultado_2=$(($numero_2%2))
resultado_3=$(($numero_3%2))

if [ resultado_1 -eq 0 ]; then
        echo "El numero " $resultado_1 " es par"
else
        echo "El numero " $resultado_1 " es impar"
fi

if [ resultado_2 -eq 0 ]; then
        echo "El numero " $resultado_2 " es par"
else
        echo "El numero " $resultado_2 " es impar"
fi

if [ resultado_3 -eq 0 ]; then
        echo "El numero " $resultado_3 " es par"
else
        echo "El numero " $resultado_3 " es impar"
fi
~  
