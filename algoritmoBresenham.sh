#!/bin/bash/
#Fecha: 18-06-21
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

x=0
y=0

algoritmoBresenham(){
	x=$a1
	y=$b1
	dx=$(($a2 - $a1))
	dy=$(($b2 - $b1))
	p=$((2*($dy) - $dx))

	while [ $x -le $a2 ]; do
		echo -e "x:"$x" y:"$y"\n"
		x=$(($x+1))

		if [ $p -lt 0 ]; then
			p=$(($p + 2 * $dy))
		else
			p=$(($p + ( 2 * $dy) - (2 * $dx)))
			y=$(($y+1))
		fi
	done
}

echo "Digite la coordenada de x1: "
read a1
echo "Digite la coordenada de y1: "
read b1
echo "Digite la coordenada de x2: "
read a2
echo "Digite la coordenada de y2: "
read b2

algoritmoBresenham $a1,$b1,$a2,$b2

