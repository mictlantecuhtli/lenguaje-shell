#!/bin/bash
#Fecha: 17-08-2020
#Autor: Fatima Azucena MC
#fatimaazucenamartinez274@gmail.com

#La compañía de autobuses “La curva loca” requiere determinar el costo que tendrá el boleto de un viaje sencillo, esto basado en los kilómetros por recorrer y en el costo por kilómetro.

echo "¿Cual es el costo del kilometro?: "
read precioKilometros
echo "¿Cuantos kilometros va a recorrer?: "
read kilometrosRecorridos

costoBoleto=$(echo "scale=4;$precioKilometros*$kilometrosRecorridos" | bc -l)

echo "El costo del boleto es de: " $costoBoleto " pesos"
