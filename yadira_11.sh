#!/bin/bash/
#Fecha: 02-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Imprime la tabla de multiplicar de un numero N
echo "¿Que tabla desea ver?: "
read tabla
echo "¿Hasta que numero desea ver?: "
read numero

for i in $(seq $numero); do
	resultado=$(($tabla*$i))
	echo $tabla" x "$i" = "$resultado
done
