#!/bin/bash/
#Fecha: 18-06-21
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

algoritmoDDA(){
	dx=$(($a2 - $a1))
	dy=$(($b2 - $b1))

	if [ $dx -gt $dy ]; then
		steps=$dx
	else
		steps=$dy
	fi

	xinc=$(($dx/$steps))
	yinc=$(($dy/$steps))

	for (( x = 0; x <= $steps; x++)) do
		echo -e "X: " $a1 " Y: " $b1 "\n"
		a1=$(($a1 + $xinc))
		b1=$(($b1 + $yinc))
	done
}

echo "Digite la coordenada de x1: "
read a1
echo "Digite la coordenada de y1: "
read b1
echo "Digite la coordenada de x2: "
read a2
echo "Digite la coordenada de y2: "
read b2

algoritmoDDA $a1,$b1,$a2,$b2


