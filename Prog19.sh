#!bin/bash/

#Fecha: 14_10_2020
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#18-Se desea saber el total de una caja registradora de un almacén, 
#se conoce el número de billetes y monedas, así como su valor. Realice 
#un algoritmo para determinar el total. Represente la solución me­diante
#el diagrama de flujo, el pseudocódigo.

i=0
resultadoFinal=0
echo "¿Cuantas monedas de distinto valor tiene?: "
read cantidadMonedas

for i in $(seq $cantidadMonedas); do
	echo "¿Cual es el valor de la moneda?: "
	read valorMoneda
	echo "Inserte el numero de monedas con ese valor: "
	read numeroMonedas
	resultadoMonedas=$(($resultadoMonedas+($valorMoneda*$numeroMonedas)))
done

echo "¿Cuantos billetes de distinto valor tienes?: "
read cantidadBilletes

for i in $(seq $cantidadBilletes); do
       echo "¿Cual es el valor de es billete?: "
       read valorBillete
       echo "Inserte el numero de billetes con ese valor: "
       read numeroBillete
       resultadoBilletes=$(($resultadoBilletes+($valorBillete*$numeroBillete)))
done
resultadoFinal=$(($resultadoMonedas+$resultadoBilletes))
echo "El total almacenado en la caja registradora es de: $"$resultadoFinal" pesos"
