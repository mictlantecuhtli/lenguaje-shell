#!/bin/bash/
#Fecha: 02-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Una persona recibe un préstamo de $10,000.00 de un banco 
#y desea saber cuánto pagará de interés, si el banco le cobra una tasa del 27% anual.

PRESTAMO=10000
INTERES=0.27

echo "Ingrese el año en que solicito el prestamo :"
read prestamo
echo "Ingrese el año actual :"
read anyo_actual
#Calcula el monto a pagar
for((x = $prestamo; x <= $anyo_actual; x++)) do
   PRESTAMO=$(echo "scale=2; $PRESTAMO + ($PRESTAMO*$INTERES)" | bc -l)
   echo "El interes del año "$x" es: $"$PRESTAMO" pesos"
done

