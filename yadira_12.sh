#!/bin/bash/
#Fecha: 10-02-21
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Tema Estructuras cíclicas
#1_EN EL TECNOLÓGICO DE JILOTEPEC SE REALIZARÁ UNA ENCUESTA, PARA CONOCER
#MAYOR INFORMACIÓN DE LOS ALUMNOS Y VERIFICAR QUIENES DE ELLOS TIENEN O
#HAN TENIDO ALGUNA DIFICULTAD DE CONECTIVIDAD.  PARA LO CUAL LES ESTAN
#HACIENDO UN CUESTIONARIO CON LAS SIGUIENTES PREGUNTAS:
# • EDAD
# • LOCALIDAD
# • TIENEN INTERNET EN CASA
# • TIENEN COMPUTADORA EN CASA
# • CUENTA CON LOS RECURSOS BÁSICOS NECESARIOS DE ALIMENTACION.
#2_RECOLECTA LOS DATOS Y PRESENTA LOS RESULTADOS CONSIDERANDO QUE SE
#DESCONOCE LA CANTIDAD DE ALUMNOS.
#3_IMPRIME LOS RESULTADOS CON NÚMEROS Y PORCENTAJES.

localidad=()
edad=()

echo "Digite el numero de alumnos: "
read cantidad_Alumnos

for (( i = 0; i < $cantidad_Alumnos; i++)) do
	echo "Digite la edad del alumno: " $i ": "
	read val
	edad[$i]=$val
	echo -e "1)Canalejas\n2)San Juan Acazuchitlan\n3)Comunidad\nDigite su opcion: "
	read loc
	localidad[$i]=$loc

	case $loc in
		1)
			canalejas=$(($canalejas + 1))
		;;
		2)
			san_juan=$(($san_juan + 1))
		;;
		3)
			com=$(($com + 1))
		;;
	esac
	
	echo -e "¿Cuenta con internet en casa?:\n1)Si\n0)No\nDigite su opcion: "
	read internet

	if [ $internet == 1 ]; then
		conInternet=$(($conInternet + 1))
	elif [ $internet == 0 ]; then 
		sinInternet=$(($sinInternat + 1))
	fi

	echo -e "¿Cuenta con computadora?:\n1)Si\n0)No\nDigite su opcion: "
	read computadora

	if [ $computadora == 1 ]; then
		conComputadora=$(($conComputadora + 1))
	elif [ $computadora == 2 ]; then
		sinComputadora=$(($sinComputadora + 1))
	fi

	echo -e "¿Cuenta con una buena alimentacion?:\n1)Si\n0)No\nDigite su opcion: "
	read alimentacion

	if [ $alimentacion == 1 ]; then
		buena_alimentacion=$(($buena_alimentacion + 1))
	elif [ $alimentacion == 0 ]; then
		mala_alimentacion=$(($mala_alimentacion + 1))
	fi

done
	porcentaje=$(echo "scale=2;100/$cantidad_Alumnos" | bc -l)
	internet_si=$(echo "scale=2;$porcentaje*$conInternet" | bc -l)
	internet_no=$(echo "scale=2;$porcentaje*$sinInternet" | bc -l)
	computadora_no=$(echo "scale=2;$porcentaje*$conComputadora" | bc -l)
	computadora_si=$(echo "scale=2;$porcentaje*$sinComputadora" | bc -l)
	alimentacion_buena=$(echo "scale=2;$porcentaje*$buena_alimentacion" | bc -l)
	alimentacion_mala=$(echo "scale=2;$porcentaje*$mala_alimentacion" | bc -l)

	echo "La encuesta se aplico a "$cantidad_Alumnos
	echo $conInternet " alumnos dijeron que contaban con internet, lo que corresponde "
	echo "a un "$internet_si" porciento"
	echo $sinInternet " alumnos dijeron que no contaban con internet, lo que corresponde "
	echo "a un "$internet_no" porciento"
	echo $buena_alimentacion " alumnos dijeron que tenian una buena alimentacion lo que"
	echo "corresponde a un "$alimentacion_buena" porciento"
	echo $mala_alimentacion " alumnos dijeron que tienen una mala alimentacion lo que"
	echo "corresponde a un "$alimentacion_mala" porciento"


