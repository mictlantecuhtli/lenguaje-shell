#!/bin/bash/
#Fecha: 06-01-2021
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Elabora un programa con condicionales anidados que solicite 3 calificaciones, 
#obtén el promedio de esas tres calificaciones, y de acuerdo al promedio que 
#se obtuvo, coloca la leyenda que le corresponde, que encontraras en la imagen 
#que te comparto con el nombre NIVEL DE DESEMPEÑO-TESJI.JPG.  
#Por ejemplo si tu promedio se encuentra entre 95 y 100 deberá aparecer la 
#leyenda "EXCELENTE" //El documento se encuentra adjunto

#promedio_General=0
#echo "Digite la calificacion que obtuvo en Fisica: "
#read calificacion_Fisica
#echo "Digite la calificacion que obtuvo en Programacion: "
#read calificacion_Programacion
#echo "Digite la calificacion qe obtuvo en Historia: "
#read calificacion_Historia
#promedio_General=$(echo "scale=2;($calificacion_Fisica+$calificacion_Programacion+$calificacion_Historia)/3" | bc -l)
#promedio_General=$(($calificacion_Fisica+$calificacion_Programacion+$calificacion_Historia))
#echo "Calificacion final "$promedio_General
#int=${promedio_General%}

echo "Digite su promedio (0-100): "
read promedio_General

if [ $promedio_General -ge 95 ] && [ $promedio_General -le 100 ]; then
	echo "EXCELENTE"
elif [ $promedio_General -ge 85 ] && [ $promedio_General -le 94 ]; then
	echo "NOTABLE"
elif [ $promedio_General -ge 75 ] && [ $promedio_General -le 84 ]; then
	echo "NOTABLE"
elif [ $promedio_General -ge 70 ]; then
	echo "DESEMPEÑO INSUFICIENTE"
fi
