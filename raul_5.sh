#!/bin/bash/
#Fecha: 04-01-2021
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Calcular el nuevo salario de un empleado si obtuvo
#un incremento del 8% sobre su salario actual y un descuento 
#de 2.5% por servicios

INCREMENTO=0.08
DESCUENTO=0.025

echo "Ingrese el nombre del empleado: "
read nombre
echo "Ingrese su salario actual: "
read salario_Actual

salario_Actual=$(echo "scale=2; $salario_Actual + ($salario_Actual*$INCREMENTO)" | bc -l)
salario_Actual=$(echo "scale=2; $salario_Actual - ($salario_Actual*$DESCUENTO)" | bc -l)

echo "El salario actual de "$nombre" es de: $"$salario_Actual" pesos"
