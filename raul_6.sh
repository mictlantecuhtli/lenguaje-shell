#!/bin/bash/
#Fecha: 06-01-2021
#Autor: Fátima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

#En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología.
#El presupuesto anual del hospital se reparte de la siguiente manera:
#Pediatría 42% y Traumatología 21%

presupuesto_Anual=0
presupuesto_Pediatria=0
presupuesto_Traumatologia=0
presupuesto_Urgencias=0

echo "Ingrese el presupuesto anual: "
read presupuesto_Anual

presupuesto_Traumatologia=$(echo "scale=2; $presupuesto_Anual*0.21" | bc -l)
presupuesto_Pediatria=$(echo "scale=2; $presupuesto_Anual*0.42" | bc -l)
presupuesto_Urgencias=$(echo "scale=2; $presupuesto_Anual*0.37" | bc -l)

echo "El presupuesto para el área de traumatología es de: $"$presupuesto_Traumatologia" pesos"
echo "El presupuesto para el área de pediatria es de: $"$presupuesto_Traumatologia" pesos"
echo "El presupuesto para el área de urgencias es de: $"$presupuesto_Urgencias" pesos"
