!/bin/bash/
#Fecha: 02-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Calcula el precio de un boleto de viaje, tomando en cuenta
#el número de kilómetros que se van a recorrer. El precio por
#Kilometro es de $20.50

PRECIOKILOMETRO=20.50
echo "Ingrese los kilometros que va a recorrer: "
read kilometrosRecorridos

costoBoleto = $kilometrosRecorridos * $PRECIOKILOMETRO

echo "Usted a recorrido "$kilometrosRecorridos" kilometros y el costo de su boleto es de: "$costoBoleto" pesos"
