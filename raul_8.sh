#!/bin/bash
#Fecha: 24_11_2020
#Autor: Fatima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

#Escriba un algoritmo que, dado el número de horas trabajadas por un empleado 
#y el sueldo por hora, calcule el sueldo total de ese empleado.

sueldoTotal = 0
precioHora = 0

echo "Ingrese el nombre del empleado: "
read nombre
echo "Ingrese las horas trabajadas: "
read hTrabajadas
echo "Ingrese el precio por hora: "
read precioHora

sueldoTotal=$(($hTrabajadas*$precioHora))

echo "El sueldo bruto de "$nombre "es de: $"$sueldoTotal "pesos"
