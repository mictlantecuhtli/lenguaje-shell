#!/bin/bash
#Fecha: 19-08-2020
#Autor: Fatima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Se requiere determinar el tiempo que tarda una persona en llegar de una ciudad a otra en bicicleta, considerando que lleva una velocKidad constante.

echo "Ingrese los kilometros recorridos: "
read kRecorridos
echo "Ingrese los K/H: "
read kHora

tiempoLlegada=$((kRecorridos/kHora))

echo "El tiempo que tarda en llegar una persona es de: "$tiempoLlegada "hrs"
