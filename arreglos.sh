#!/bin/bash

area[3][8]=23
area[13]=37
area[51]=UFOs
# No hace falta que sean consecutivos.

echo -n "area[11] = "
echo ${area[11]}    #  Se necesitan {llaves}.

echo -n "area[13] = "
echo ${area[13]}

echo "Contents of area[51] are ${area[51]}."

area[5]=$((${area[11]}+${area[13]}))

area2=( zero one two three four )
# otra manera de inicializar un array
echo ${area2[0]}

area3=([17]=seventeen [24]=twenty-four)
# y otra manera, esta vez, indicando la posición de los elementos
echo ${area3[17]}

echo ${#area3[@]}
# Longitud del array

