#!/bin/bash/
#Fecha: 02-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Calcular el promedio de N edades

suma_Edades=0
edad=()

echo "¿Cuantos alumnos son?: "
read cantidad_Alumnos

for (( x = 1; x <= $cantidad_Alumnos; x++)) do
	echo "Digite la edad del alumno "$x": "
	read edad_Alumno
	edad[$x]=$edad_Alumno
	suma_Edades=$(($suma_Edades + ${edad[$x]}))
done

suma_Edades=$(($suma_Edades/$cantidad_Alumnos))
echo "El promedio de las edades es de: "$suma_Edades"%"
