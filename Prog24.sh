#!/bin/bash

#Fecha: 25-08-2020
#Autor: Fatima Azucena MC
#fatimaazucenamartinez274@gmail.com

#La CONAGUA requiere determinar el pago que debe realizar una persona por el total de metros cúbicos que consume de agua. 

echo "Ingrese el precio por metro cubico: "
read precioMetros
echo "Ingrese los metros cubicos de agua consumidos: "
read metros

resultado=$(echo "scale=3;$precioMetros*$metros" | bc -l)

echo "El total a pagar por los " $metros " cubicos de agua es de: $" $resultado " pesos"
