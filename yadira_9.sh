#Fecha: 08-01-2021
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Genere el factorial de un numero

numero_Factorial=1
i=1

echo "Digite un numero: "
read numero

for i in $(seq $numero); do
	numero_Factorial=$(($numero_Factorial*$i))
done	
echo "El factorial de "$numero" es: "$numero_Factorial
