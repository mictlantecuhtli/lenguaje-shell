#!/bin/bash/
#Fecha: 02-10-20
#Autor: Fátima Azucena MC
#fatimaazucenamartinez274@gmail.com

#Calcula la sumatoria de N numeros

numeros=()
sumatoria=0

echo "¿Cuantos numeros desea sumar?: "
read cantidad_Numeros

for (( i = 1; i <= $cantidad_Numeros; i++)) do
	echo "Digite el numero "$i": "
	read numero
	numeros[$i]=$numero
	sumatoria=$(($sumatoria + ${numeros[$i]}))
done

echo "La sumatoria es de: "$sumatoria
