#!/bin/bash
#Fecha: 04_09_2020
#Autor:Fatima Azucena MC
#fatimaazucenamartinez274@gmail.com 

#Un vendedor ha realizado N ventas y desea saber cuántas fueron por 10,000 o menos, 
#cuántas fueron por más de 10,000 pero por menos de 20,000, y cuánto fue el monto global.

#declare -a ventas
ventas=[]
i=1
montoGlobal=0
montoMADE=0
montoMEDE=0

echo "Ingrese el numero de ventas: "
read numVentas

for i in $(seq $numVentas); do
	pos=$(($pos+1))
	echo "Ingrese el valor de la venta "$pos": "
	read valorVenta
	ventas[$pos]=$valorVenta
	montoGlobal=$(($montoGlobal+${ventas[$pos]}))

	if [ ${ventas[$pos]} -ge 10000 ] && [ ${ventas[$pos]} -le 20000 ]; then
		montoMEDE=$(($montoMEDE+${ventas[$pos]}))
	elif [ ${ventas[$pos]} -gt 10000 ] && [ ${ventas[$pos]} -le 20000 ]; then
		montoMADE=$(($montoMADE+${ventas[$pos]}))
	fi
done

echo "El monto global es de "$montoGlobal" pesos"
