#!/bin/bash
#Fecha: 28_12_2020
#Autor: Fatima Azucena MC
#Correo: fatimaazucenamartinez274@gmail.com

#Un vendedor recibe un sueldo base más un 10% por comision de sus ventas
#el vendedor desea saber cuanto dinero obtendra por concepto de comisiones
#por las 3 ventas que realiza en el mes y el total que recibira en el mes
#tomando en cunta su base y comisiones

comision=0
sueldoFijo=0
sueldo_Total=0
venta_1=0
venta_2=0
venta_3=0
total_Venta=0
PORCENTAJE=0.10
echo "Ingrese su sueldo base: "
read sueldoFijo
echo "Ingrese el valor de la venta 1: "
read venta_1
echo "Ingrese el valor de la venta 2: "
read venta_2
echo "Ingrese el valor de la venta 3: "
read venta_3
total_Venta=$(($venta_1+$venta_2+$venta_3))
comision=$(echo "scale=2; $comision + ($total_Venta*$PORCENTAJE)" | bc -l)
sueldo_Total=$(($comision+$sueldoFijo))

echo "El sueldo total es de: $"$sueldo_Total" y el total por concepto de comisiones es de: $"$comision" pesos"


